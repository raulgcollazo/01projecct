<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DAM 2 Práctica 1 09/2016</title>
        <!-- Añade el enlace a la hoja de estilos de Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

        <!-- Estilo personalizado -->
        <link href="miestilo.less" rel="stylesheet">


    </head>
    
  
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        
        
         <?php require('cabecera.php')?>

 <form class="form-horizontal">
<fieldset>

<!-- Form Name -->
<legend>FORM ALUMNOS</legend>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="textinput">MARCA</label>  
  <div class="col-md-4">
  <input id="textinput" name="textinput" type="text" placeholder="MARCA" class="form-control input-md">
  <span class="help-block">MARCA</span>  
  </div>
</div>

<!-- Text input-->
<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">MODELO</label>  
  <div class="col-md-4">
  <input id="MODELO" name="MODELO" type="text" placeholder="MODELO" class="form-control input-md">
  <span class="help-block">MODELO</span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">PROCESADOR</label>  
  <div class="col-md-4">
  <input id="PROCESADOR" name="PROCESADOR" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">PROCESADOR</span>  
  </div>
</div>



<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">RAM</label>  
  <div class="col-md-4">
  <input id="RAM" name="PROCESADOR" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">RAM</span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">BATERIA</label>  
  <div class="col-md-4">
  <input id="BATERIA" name="PROCESADOR" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">BATERIA</span>  
  </div>
</div>


<div class="form-group">
  <label class="col-md-4 control-label" for="MODELO">PANTALLA</label>  
  <div class="col-md-4">
  <input id="PANTALLA" name="PROCESADOR" type="text" placeholder="" class="form-control input-md">
  <span class="help-block">PANTALLA</span>  
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="RAM">RAM</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="RAM-0">
      <input type="radio" name="RAM" id="RAM-0" value="1" checked="checked">
      1 GB
    </label>
	</div>
  <div class="radio">
    <label for="RAM-1">
      <input type="radio" name="RAM" id="RAM-1" value="2">
      2 GB
    </label>
	</div>
  </div>
</div>

<!-- Multiple Radios -->
<div class="form-group">
  <label class="col-md-4 control-label" for="">PROCESADOR</label>
  <div class="col-md-4">
  <div class="radio">
    <label for="-0">
      <input type="radio" name="" id="-0" value="1" checked="checked">
      QUAD CORE
    </label>
	</div>
  <div class="radio">
    <label for="-1">
      <input type="radio" name="" id="-1" value="2">
      OCTA CORE
    </label>
	</div>
  </div>
</div>

</fieldset>
</form>

      
      
       <?php require('pie.php');?>
        

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
