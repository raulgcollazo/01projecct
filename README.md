# PROYECTO 1 #

Punto de partida para realizar el proyecto que engloba los capítulos enumerados a continuación:
# De la asignatura de Desarrollo Web #
*	capítulo 1 introducción a PHP
*	capítulo 2 Formularios, métodos Get y Post
*	capítulo 3 Conexión con MySQl 
# De la asignatura de Desarrollo de Interfaces Web #
*	capítulo 1 Accesibilidad y Usabilidad, Layouts Bootstrap
*	capítulo 2 Diseño y estilos, Less y componentes de bootstrap 