var SmartCarousel = function(options){
	var carouselElement = document.getElementById(options.carouselId);
	var totalCarouselItems = carouselElement.childElementCount;
	var indicatorsAreVisible = options.showIndicators || false;
	var controlsAreVisible = options.showControls || false;
	var intervalDelay = options.intervalDelay || 2000;
	var hammerJsIsPresent = (typeof Hammer != 'undefined') ? true : false;

	(function(){
		carouselElement.setAttribute('class', 'carousel slide');
		
		if(indicatorsAreVisible){
			buildIndicators();
		}
		
		createInnerCarouselElement();
		addItemsToInnerCarousel();
		
		if(controlsAreVisible){
			buildControls();
		}

		$('#' + carouselElement.id).carousel({
			interval: intervalDelay
		});

		if(hammerJsIsPresent){
			addMultiTouchGestureEvents();
		}
	})();

	function buildIndicators(){
		var i;
		var olElement = document.createElement('ol');

		olElement.setAttribute('class','carousel-indicators');

		for(i = 0; i < totalCarouselItems; i++){
			var liElement = document.createElement('li');
			liElement.setAttribute('data-target','#' + carouselElement.id);
			liElement.setAttribute('data-slide-to', i);
			if(i == 0){
				liElement.setAttribute('class', 'active');
			}
			olElement.appendChild(liElement);
		}
		carouselElement.appendChild(olElement);
	}

	function createInnerCarouselElement(){
		var innerCarouselElement = document.createElement('div');
		innerCarouselElement.setAttribute('class','carousel-inner');
		carouselElement.appendChild(innerCarouselElement);
	}

	function addItemsToInnerCarousel(){
		var i;
		var itemElements = carouselElement.querySelectorAll('#' + carouselElement.id + ' > div');
		var carouselInnerElement = carouselElement.querySelector('div.carousel-inner');
		
		for(i = 0; i < itemElements.length; i++){
			if(itemElements[i].getAttribute('class') == null){
				itemElements[i].setAttribute('class',((i == 0) ? 'active item' : 'item'));
				carouselInnerElement.appendChild(itemElements[i]);
			}
		}
	}

	function buildControls(){
		var leftControlElement = document.createElement('a');
		var rightControlElement = document.createElement('a');

		leftControlElement.setAttribute('class','carousel-control left');
		leftControlElement.setAttribute('href','#' + carouselElement.id);
		leftControlElement.setAttribute('data-slide','prev');
		leftControlElement.innerHTML = "&lsaquo;";
		carouselElement.appendChild(leftControlElement);

		rightControlElement.setAttribute('class','carousel-control right');
		rightControlElement.setAttribute('href','#' + carouselElement.id);
		rightControlElement.setAttribute('data-slide','next');
		rightControlElement.innerHTML = "&rsaquo;";
		carouselElement.appendChild(rightControlElement);
	}

	function addMultiTouchGestureEvents(){
		Hammer(carouselElement).on("dragleft swipeleft", function(event) {
	        $('#' + carouselElement.id).carousel('next');
	    });

	    Hammer(carouselElement).on("dragright swiperight", function(event) {
	        $('#' + carouselElement.id).carousel('prev');
	    });
	}

	return this;
}
