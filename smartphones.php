<!DOCTYPE html>
<html lang="en">

    <head>

        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="">
        <meta name="author" content="">

        <title>DAM 2 Práctica 1 09/2016</title>
        <!-- Añade el enlace a la hoja de estilos de Bootstrap -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" >

        <!-- Estilo personalizado -->
        <link href="miestilo.less" rel="stylesheet">


    </head>
    
  
    <body id="page-top" data-spy="scroll" data-target=".navbar-fixed-top">
        
        
         <?php require('cabecera.php')?>
<div class="jumbotron">
            <div class="container">
 <?php
                $alumnosJson = file_get_contents('alumnos.json');
                //    var_dump($alumnosJson);die;
                // para experimentar... 
                // modifica el nombre del archivo. ¿Que error da?
                // Si modificamos la ruta a /data del archivo JSon deberemos actualizar el path
                // Pdemos realizar la lectura del archivo y decodificarlo como JSon
                $alumnosJson = file_get_contents('alumnos.json');
                //   $alumnos = json_decode($alumnosJson); 
                //    var_dump($alumnos);die
                // Si realizamos de esta manera la impresión da error,
                // debemos añadirle un segundo parametro al decodificador
                $alumnos = json_decode($alumnosJson, true);
                ?>   


                <div class="row">
                    <?php foreach ($alumnos as $participante) { ?>
                        <div class="col-lg-4 clase-list-item">
                            <h2><?php echo $participante['nombre']; ?></h2>

                            <blockquote class="clase-details">
                                <span class="label label-info">
                                    <?php //echo $participante['sexo']; ?></span>
                                    <?php //echo $participante['edad']; ?>
                                <?php //echo $participante['poblacion']; ?> 
                                <!-- 7. Añadiendo imagenes -->
                                <img src="data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAOEAAADhCAMAAAAJbSJIAAAAYFBMVEX///8BAQMAAAD7+/s3Nzc2Njjx8fLHx8eioqLl5eWVlZXY2Nj///0KCguioqT29vfPz9Dr6+tZWVpHR0d1dXVtbW9TU1OKioqenp5NTU63t7eBgYHV1dXf398rKysBAAYBXq7YAAAC2UlEQVR4nO3YbVuiQBhAYZzaljQx382S/v+/XLLW6MvmwuGBuM757oz3xQAzZFnzTsWunKdJt6V5uStOLf5lC982vdUx8M1YtZ3FA/MQ3SfyEA3cB/rejftY4CEYGH4VZ5FL9EKMvBdX0b6zcRUHXIZfwbMwLcOEeR/AipiHCTc9CTdhwnVPwnUU8Oa2J+HtTZTwl0KFCn+SMH3fzxam3fS7dlcbhyn8/kBw/TFlmMLn5f2/W+6vH2yQwtHfh9cYf7oQTKFChQoVKlSosBPhNbtOpL6EaVoUeecVRTFNfQkXQRMu3q9iuDClu6AJ7xTSKcRTiKcQTyGeQjyFeArxFOIpxFOIpxBPIZ5CPIV4CvEU4inEU4inEE8hnkI8hXgK8RTiKcRTiKcQTyGeQjyFeArxFOIpxFOIpxBPIZ5CPIV4CvEU4inEU4inEE8hnkI8hXgK8RTiKcRTiKcQTyGeQjyFeArxFOIpxFOIpxBPIZ5CPIV4CvEU4inEU4inEE8hnkI8hXgK8RTiKcRTiKcQTyGeQjyFeArxFOIpxFOIpxBPIZ5CPIV4CvEU4inEU4inEE8hnkI8hXgK8RTiKcRTiKcQTyHeh3CSFkETLnoTHn/HdPyYL1xYrdOgJn0Jg1OoUKFChQrHIry8vkcq/LJFGaEwpTKfnWZ52dA4eGFK+48Bps2IwxdOLyNMRylMT7UhnhqNMHThsTbEcYTClJa1IZZN7sTBC+t/72aUwvp3q7sRCifpsTbE4wjvw0na1IbYjFKYXi4jvDR65Q9dWBH/rtPHke5pKuLzffXz++ex7kvPR4vytmx8uIgTZuvGZ7w2h6dJWkcBmz0J2/fladxtRU/CPEzYaFfZHvhlZ9txq3DhayVcxQGzWZsPSg2FKc0ChdkhfJ2mdIgEZtk+mPj5kSesPHKhpvMVfHiIJZ62rV7f/8VL29MbMFhYGfNdOe8cOC93+ek8XzPgH/CvUY+yHLDVAAAAAElFTkSuQmCC">
                            </blockquote>
                            <p> 
                                <!--8. Maravilloso IF-->
                            <blockquote class="alumno-details">  
                                <span class="label label-info">
                                    <?php echo $participante['sexo']; ?></span>
                                   
                                    <?php
                                    // En caso de que exista la la llave edad lo imprimimos
                                         if (array_key_exists('edad', $participante)) {
                                              echo $participante['edad'];    
                                         } else {  
                                             // De lo contrario...
                                             echo 'joven';
                                         }

                                    ?>
                                    <?php
                                  // En caso de que dispongamos valor para la poblacion
                                        if (array_key_exists('poblacion', $participante))
                                        {
                                            if ($participante['poblacion'] != '')
                                            {        
                                                echo $participante['poblacion'];
                                            } else {
                                                echo 'Sin especificar';
                                            }
                                        } else {
                                            echo 'Sin casa'; }
                                    // Podemos implementar los If de manera conjunta
                                       if (array_key_exists('poblacion', $participante) && $participante['poblacion'] != '')
                                       { 
                                          // echo $participante['poblacion'];
                                       } 
                                       else
                                       {
                                         //  echo 'Desconocido'; 
                                       }
                                    ?>
                            </blockquote>

                            </p>
                        </div>


                    <?php } ?>
</div>
</div>

      
      
       <?php require('pie.php');?>
        

     <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

</body>

</html>
